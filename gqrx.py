#!/usr/bin/env python3

from src.arguments.parse import parse_config
from src.client.gqrx import GqrxClient
from src.database.db import DB
from controller import Controller

SCRIPT_VERSION = 'v0.8'


if __name__ == '__main__':
    print(f'Script info: {SCRIPT_VERSION}')

    config = parse_config()
    gqrx_client = GqrxClient(config.host, config.port)
    database = DB('frequency.db')

    controller = Controller(gqrx_client, database, config)

    cycles = config.cycles
    try:
        while cycles != 0:
            controller.scan()
            if cycles > 0:
                cycles -= 1
    except KeyboardInterrupt:
        print('Exiting!')
    finally:
        database.close()
        exit()

