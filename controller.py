from datetime import datetime
from threading import Thread
from time import sleep, time

from beepy import beep

from src.client.gqrx import GqrxClient
from src.database.db import DB
from src.domain.config import Config
from src.domain.scan_range import ScanRange
from src.domain.signal import Signal


class Controller:

    def __init__(self, client: GqrxClient, database: DB, config: Config):
        self.config = config
        self.database = database
        self.client = client
        self.detected_signals = []
        self.ignored_signals = []

    def scan(self):
        print(self.config)
        self.detected_signals = self.database.read_all_frequencies()
        self.ignored_signals = self.database.read_all_ignored()
        for range_ in self.config.ranges:
            print(range_)
            sleep(self.config.delay)
            self._scan_single(range_)

    def _scan_single(self, range_: ScanRange):
        self.client.set_mode(range_.mode, range_.bandwidth)
        for frequency in range_:
            if any(map(lambda s: s.start < frequency < s.finish, self.ignored_signals)):
                continue
            assert self.client.set_frequency(frequency)
            sleep(self.config.delay)
            level = self.client.get_signal_strength(self.config.aggregation, self.config.sample_size)
            signal = Signal(level, frequency, range_.bandwidth, int(time()))
            print(signal)
            if signal.strength > range_.threshold:
                self._on_detect(signal)

    def _on_detect(self, signal: Signal):
        self._notify(signal.frequency)
        if self.config.auto_scan:
            self._save_signal(signal)
        else:
            action = input('S - save | I - ignore | empty to skip\n').upper()
            if action == 'S':
                if self.config.use_comments:
                    comment = input('Comment: ')
                    signal = Signal(signal.strength, signal.frequency, signal.bandwidth, signal.timestamp, comment)
                self._save_signal(signal)
            elif action == 'I':
                self._ignore_signal(signal)
            else:
                print('Skip')

    def _notify(self, frequency: int):
        if self.config.notify:
            Thread(target=beep, args=[0]).start()
        if self.config.verbose:
            signals = list(filter(lambda s: s.start < frequency < s.finish, self.detected_signals))
            signals_num = len(signals)
            if signals_num > 0:
                print(f'This frequency was detected {signals_num} times:')
                for signal in signals:
                    dt = datetime.fromtimestamp(signal.timestamp)
                    print(f'\tat {dt} it was {signal.strength} dB')

    def _save_signal(self, signal: Signal):
        print('\u001b[32mSaving \u001b[0m')
        self.database.save_frequency(signal)

    def _ignore_signal(self, signal: Signal):
        print('\u001b[32mIgnoring \u001b[0m')
        self.database.save_ignored(signal)
