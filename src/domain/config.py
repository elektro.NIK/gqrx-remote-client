from dataclasses import dataclass
from typing import List

from .scan_range import ScanRange
from .aggregation import Aggregation


@dataclass
class Config:
    host: str
    port: int
    ranges: List[ScanRange]
    delay: float
    sample_size: int
    aggregation: Aggregation
    cycles: int
    auto_scan: bool
    notify: bool
    verbose: bool
    use_comments: bool

    def __str__(self):
        return '--------------- Configuration ---------------\n' + \
               f'| Host: {self.host}:{self.port}\n' + \
               f'| Delay: {self.delay} s\n' + \
               f'| Cycles: {self.cycles}\n' + \
               f'| Processing: {self.aggregation} of {self.sample_size} values\n' + \
               f'| Auto Scan: {self.auto_scan}\n' + \
               f'| Notify: {self.notify}\n' + \
               f'| Use Comments: {self.use_comments}\n' + \
               '---------------------------------------------'
