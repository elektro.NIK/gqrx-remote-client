from dataclasses import dataclass


@dataclass
class Signal:
    strength: float
    frequency: int
    bandwidth: int
    timestamp: int
    comment: str = ''

    @property
    def start(self):
        return self.frequency - self.bandwidth // 2

    @property
    def finish(self):
        return self.frequency + self.bandwidth // 2

    def __str__(self):
        num_flakes = '|' + '#' * int(self.strength + 50)
        return f'{(self.frequency / 1E6):.2f} MHz: {self.strength} dB\t' + num_flakes
