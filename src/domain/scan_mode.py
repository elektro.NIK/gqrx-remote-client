from __future__ import annotations
from enum import Enum


class ScanMode(Enum):
    OFF = 'OFF'
    RAW = 'RAW'
    AM = 'AM'
    AM_SYNC = 'AMS'
    LSB = 'LSB'
    USB = 'USB'
    CW_L = 'CWL'
    CW_R = 'CWR'
    CW_U = 'CWU'
    CW = 'CW'
    FM = 'FM'
    WFM = 'WFM'
    WFM_STEREO = 'WFM_ST'
    WFM_STEREO_OIRT = 'WFM_ST_OIRT'

    @staticmethod
    def from_str(mode: str) -> ScanMode:
        for m in ScanMode:
            if m.value == mode.upper():
                return m
        else:
            raise Exception(f'Invalid mode: {mode}')

    def __str__(self):
        return self.value

