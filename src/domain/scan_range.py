from dataclasses import dataclass

from .scan_mode import ScanMode


@dataclass
class ScanRange:
    start: float
    finish: float
    step: float
    threshold: float
    mode: ScanMode
    bandwidth: int

    def __str__(self):
        return '------------ Range Info ------------\n' + \
               f'| Range: {self.start} -- {self.finish} MHz\n' + \
               f'| Step: {self.step} MHz\n' + \
               f'| Threshold: {self.threshold} dB\n' + \
               f'| Bandwidth: {self.bandwidth} kHz\n' + \
               f'| Mode: {self.mode}\n' + \
               '------------------------------------'

    def __iter__(self):
        start = int(self.start * 1E6)
        finish = int(self.finish * 1E6)
        step = int(self.step * 1E6)
        return iter(range(start, finish + 1, step))
