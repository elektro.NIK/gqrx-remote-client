from __future__ import annotations
from enum import Enum


class Aggregation(Enum):
    MAX = 'MAX'
    AVG = 'AVG'

    @staticmethod
    def from_str(mode: str) -> Aggregation:
        for m in Aggregation:
            if m.value == mode.upper():
                return m
        else:
            raise Exception(f'Invalid mode: {mode}')

    def __str__(self):
        return self.value
