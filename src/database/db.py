from time import time
import sqlite3
from typing import List

from src.domain.signal import Signal


def _map_data_to_signals(rows):
    signals = []
    for row in rows:
        bandwidth = row[1] - row[0]
        frequency = row[0] + bandwidth // 2
        strength = row[2]
        timestamp = row[3]
        comment = row[4]
        signals.append(Signal(strength, frequency, bandwidth, timestamp, comment))
    return signals


def _map_ignored_to_signals(rows):
    signals = []
    for row in rows:
        bandwidth = row[1] - row[0]
        frequency = row[0] + bandwidth // 2
        timestamp = row[2]
        signals.append(Signal(0, frequency, bandwidth, timestamp))
    return signals


class DB:

    def __init__(self, filename: str):
        self.filename = filename
        self.connection = sqlite3.connect(self.filename)
        self.connection.execute(FrequencyQuery.CREATE_TABLE)
        self.connection.execute(IgnoredQuery.CREATE_TABLE)

    def get_all_signals_of_frequency(self, frequency: float) -> List[Signal]:
        query = FrequencyQuery.SELECT_WHERE_FREQUENCY.format(frequency)
        rows = self.connection.execute(query)
        return _map_data_to_signals(rows)

    def read_all_frequencies(self) -> List[Signal]:
        query = FrequencyQuery.SELECT_ALL
        rows = self.connection.execute(query)
        return _map_data_to_signals(rows)

    def read_all_ignored(self) -> List[Signal]:
        query = IgnoredQuery.SELECT_ALL
        rows = self.connection.execute(query)
        return _map_ignored_to_signals(rows)

    def save_frequency(self, s: Signal) -> None:
        query = FrequencyQuery.INSERT.format(s.start, s.finish, s.strength, s.timestamp, s.comment)
        self.connection.execute(query)
        self.connection.commit()

    def save_ignored(self, signal: Signal) -> None:
        query = IgnoredQuery.INSERT.format(signal.start, signal.finish, signal.timestamp)
        self.connection.execute(query)
        self.connection.commit()

    def close(self):
        self.connection.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


class FrequencyQuery:
    CREATE_TABLE = 'CREATE TABLE IF NOT EXISTS frequency (start_ INT, finish_ INT, signal FLOAT, timestamp_ INT, comment VARCHAR(200))'
    INSERT = 'INSERT INTO frequency VALUES ({}, {}, {}, {}, "{}")'
    SELECT_ALL = 'SELECT * FROM frequency'
    SELECT_WHERE_FREQUENCY = 'SELECT * FROM frequency WHERE {} BETWEEN frequency.start_ AND frequency.finish_'


class IgnoredQuery:
    CREATE_TABLE = 'CREATE TABLE IF NOT EXISTS ignored (start_ INT, finish_ INT, timestamp_ INT)'
    INSERT = 'INSERT INTO ignored VALUES ({}, {}, {})'
    SELECT_ALL = 'SELECT * FROM ignored'
