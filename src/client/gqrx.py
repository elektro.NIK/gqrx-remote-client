from __future__ import annotations
import telnetlib
from typing import Tuple
from enum import Enum

from src.domain.aggregation import Aggregation
from src.domain.scan_mode import ScanMode


class GqrxClient:
    _GET_FREQ = 'f'
    _SET_FREQ = 'F {}'
    _GET_MODE = 'm'
    _SET_MODE = 'M {}'
    _SET_MODE_BAND = 'M {} {}'
    _GET_STRENGTH = 'l STRENGTH'
    _GET_THRESHOLD = 'l SQL'
    _GET_LNA_GAIN = 'l LNA_GAIN'
    _SET_THRESHOLD = 'L SQL {}'
    _SET_LNA_GAIN = 'L LNA_GAIN {}'
    _GET_RDS_PI_CODE = 'p RDS_PI'
    _GET_RECORD_STATUS = 'u RECORD'
    _GET_DSP_STATUS = 'u DSP'
    _GET_RDS_STATUS = 'u RDS'
    _SET_RECORD_STATUS = 'U RECORD {}'
    _SET_DSP_STATUS = 'U DSP {}'
    _SET_RDS_STATUS = 'U RDS {}'
    _GET_FREQ_SHIFT = 'LNB_LO'
    _SET_FREQ_SHIFT = 'LNB_LO {}'
    _VERSION = '_'

    def __init__(self, host: str = 'localhost', port: int = 7356):
        self.tn = telnetlib.Telnet(host, port)
        self.cut_level = self.get_threshold()

    def _write_line(self, line: str) -> None:
        self.tn.write(line.encode('ascii') + b'\r\n')

    def _read_line(self) -> str:
        return self.tn.read_some().decode('ascii')

    def _get_response(self) -> bool:
        response = self._read_line()
        head, code = response.split()
        if head != 'RPRT':
            raise Exception(f'Unknown response: {response}')
        return int(code) == 0

    def get_frequency(self) -> int:
        self._write_line(self._GET_FREQ)
        freq = self._read_line()
        return int(freq)

    def set_frequency(self, freq: int) -> bool:
        self._write_line(self._SET_FREQ.format(freq))
        return self._get_response()

    def get_mode(self) -> Tuple[Enum, int]:
        self._write_line(self._GET_MODE)
        mode, bandwidth = self._read_line().split()
        return ScanMode.from_str(mode), int(bandwidth)

    def set_mode(self, mode: ScanMode, bandwidth: int = None):
        if bandwidth is None:
            self._write_line(self._SET_MODE.format(mode.value))
        else:
            self._write_line(self._SET_MODE_BAND.format(mode.value, bandwidth))
        return self._get_response()

    def get_signal_strength(self, aggregation: Aggregation, sample_size: int) -> float:
        sample = []
        for _ in range(sample_size):
            self._write_line(self._GET_STRENGTH)
            lvl = self._read_line()
            sample.append(float(lvl))

        if aggregation == Aggregation.MAX:
            return max(sample)
        elif aggregation == Aggregation.AVG:
            return sum(sample) / sample_size
        else:
            return sample[0]

    def get_threshold(self) -> float:
        self._write_line(self._GET_THRESHOLD)
        lvl = self._read_line()
        return float(lvl)

    def get_lna_gain(self) -> float:
        self._write_line(self._GET_LNA_GAIN)
        lvl = self._read_line()
        return float(lvl)

    def set_threshold(self, value: float) -> bool:
        self._write_line(self._SET_THRESHOLD.format(value))
        return self._get_response()

    def set_lna_gain(self, value: float) -> bool:
        self._write_line(self._SET_LNA_GAIN.format(value))
        return self._get_response()

    def get_rds_pi_code(self) -> int:
        self._write_line(self._GET_RDS_PI_CODE)
        lvl = self._read_line()
        return int(lvl, 16)

    def recording_is_active(self) -> bool:
        self._write_line(self._GET_RECORD_STATUS)
        status = self._read_line()
        return '0' != status

    def record_start(self) -> bool:
        self._write_line(self._SET_RECORD_STATUS.format(1))
        return self._get_response()

    def record_stop(self) -> bool:
        self._write_line(self._SET_RECORD_STATUS.format(0))
        return self._get_response()

    def transceiver_is_active(self) -> bool:
        self._write_line(self._GET_DSP_STATUS)
        status = self._read_line()
        return '0' != status

    def transceiver_start(self) -> bool:
        self._write_line(self._SET_DSP_STATUS.format(1))
        return self._get_response()

    def transceiver_stop(self) -> bool:
        self._write_line(self._SET_DSP_STATUS.format(1))
        return self._get_response()

    def fm_rds_is_active(self) -> bool:
        self._write_line(self._GET_RDS_STATUS)
        status = self._read_line()
        return '0' != status

    def fm_rds_start(self) -> bool:
        self._write_line(self._SET_RDS_STATUS.format(1))
        return self._get_response()

    def fm_rds_stop(self) -> bool:
        self._write_line(self._SET_RDS_STATUS.format(1))
        return self._get_response()

    def get_freq_shift(self) -> int:
        self._write_line(self._GET_FREQ_SHIFT)
        shift = self._read_line()
        return int(shift)

    def set_freq_shift(self, shift: int = 0) -> bool:
        self._write_line(self._SET_FREQ_SHIFT.format(shift))
        return self._get_response()

    def get_version(self) -> str:
        self._write_line(self._VERSION)
        return self._read_line()
