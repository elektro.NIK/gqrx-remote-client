import argparse
import yaml

from src.domain.aggregation import Aggregation
from src.domain.config import Config
from src.domain.scan_mode import ScanMode
from src.domain.scan_range import ScanRange


def parse_config() -> Config:
    argp = argparse.ArgumentParser(description='GQRX remote control')
    argp.add_argument('--config', '-C', help='path to config file', type=str, required=True)
    args = argp.parse_args()
    with open(args.config) as file:
        data = yaml.safe_load(file)
    return _parse_yaml(data)


def _parse_yaml(data) -> Config:
    ranges = []
    for dto in data['ranges']:
        scan_range = ScanRange(
            start=dto['start'],
            finish=dto['finish'],
            step=dto['step'],
            mode=ScanMode.from_str(dto['mode']),
            bandwidth=dto['bandwidth'],
            threshold=dto['threshold'],
        )
        ranges.append(scan_range)
    return Config(
        host=data['host'],
        port=data['port'],
        delay=data['delay'],
        sample_size=data['sample_size'],
        aggregation=Aggregation.from_str(data['aggregation']),
        cycles=data['cycles'],
        auto_scan=data['auto_scan'],
        notify=data['notify'],
        verbose=data['verbose'],
        use_comments=data['comments'],
        ranges=ranges,
    )



