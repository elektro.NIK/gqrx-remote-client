#!/usr/bin/env python3
from datetime import datetime

from src.database.db import DB
from src.domain.signal import Signal

DB_NAME = 'frequency.db'
FREQUENCY_CSV_NAME = 'frequency.csv'
IGNORED_CSV_NAME = 'ignored.csv'


def to_signal_csv(signal: Signal):
    dt = datetime.fromtimestamp(signal.timestamp)
    return f'{(signal.frequency / 1E6):.2f},' \
           f'{(signal.bandwidth / 1E3):.2f},' \
           f'{signal.strength},' \
           f'{dt.date()},' \
           f'{dt.time()},' \
           f'{signal.comment}\n'


def to_ignored_csv(signal: Signal):
    dt = datetime.fromtimestamp(signal.timestamp)
    return f'{(signal.frequency / 1E6):.2f},{(signal.bandwidth / 1E3):.2f},{dt.date()},{dt.time()}\n'


if __name__ == '__main__':
    with DB(DB_NAME) as db:
        signals = db.read_all_frequencies()
        ignored_signals = db.read_all_ignored()
        print('Got data from database')
        with open(FREQUENCY_CSV_NAME, 'w') as file:
            print(f'Exporting data to {FREQUENCY_CSV_NAME}...')
            file.write('frequency (MHz),bandwidth (kHz),signal_level,date,time,comment\n')
            for s in signals:
                file.write(to_signal_csv(s))
        with open(IGNORED_CSV_NAME, 'w') as file:
            print(f'Exporting data to {IGNORED_CSV_NAME}...')
            file.write('frequency (MHz),bandwidth (kHz),date,time\n')
            for s in ignored_signals:
                file.write(to_ignored_csv(s))
    print('Done!')
